from django.db import models

# Create your models here.
from django.contrib.auth.models import User

class Courier(models.Model):
    courier_name   = models.CharField(max_length=250 )
    created_by     = models.ForeignKey(User , null=True , blank=True , on_delete= models.SET_NULL)
    created_at     = models.DateTimeField(auto_now_add=True ,blank=True)
    last_update_by = models.ForeignKey(User , null=True , blank=True , on_delete= models.SET_NULL,
                                                                 related_name= "last_user_update") 
    Last_update_at = models.DateTimeField(blank=True , null=True)
    def __str__(self) :
        return self.courier_name