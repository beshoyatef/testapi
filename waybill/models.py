from ast import mod
from django.db import models
from courier.models import Courier
import datetime
# Create your models here.

WaybillStatus = [("Cancele" ,"Cancele") ,
        ("Returned","Returned") , 
        ("Delivere","Delivere") , 
        ("Out for delivery" ,"Out for delivery") ,
        ("Confirme","Confirme") , 
        ("Created","Created")]
class Waybill(models.Model) :
    courier = models.ForeignKey(Courier , on_delete=models.CASCADE , null=True , blank=True)
    posting_data = models.DateTimeField(auto_now_add=True , blank=True)
    customer_name = models.CharField(max_length=250 , blank= True , null=True)
    phonenumber = models.CharField(max_length=20 , null=True ,blank=True)
    phonenumber_tow = models.CharField(max_length=20 , null=True ,blank=True)
    country = models.CharField(max_length=250 , blank= True , null=True)
    city  =models.CharField(max_length=250 , blank= True , null=True)
    area = models.CharField(max_length=250 , blank= True , null=True)
    street = models.CharField(max_length=250 , blank= True , null=True)
    building_number = models.CharField(max_length=250 , blank= True , null=True)
    address_one = models.CharField(max_length=700 , blank= True , null=True)
    address_tow = models.CharField(max_length=700 , blank= True , null=True)
    delivery_fees = models.DecimalField(decimal_places=2 , max_digits=50 ,blank=True , null=True)
    order_fees = models.DecimalField(decimal_places=2 ,max_digits=50, blank=True , null=True)
    grand_total = models.DecimalField(decimal_places=2 ,max_digits=50, blank=True , null=True)
    waybill_status = models.CharField(max_length=250 , choices= WaybillStatus , null=True , blank=True)
    item = models.CharField(max_length=250 , null=True , blank=True)
    updated_date = models.DateTimeField(null=True , blank=True)
    def save(self, *args, **kwargs):
        if self.id :
            self.updated_date = datetime.datetime.now()
        super(Waybill, self).save(*args, **kwargs)
    def __str__(self):
        return str(self.id or "#") + str(self.courier.courier_name or "No Courier !")
