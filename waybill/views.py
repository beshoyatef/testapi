from django.shortcuts import render

from.models import Waybill
from rest_framework import serializers ,viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions

import datetime

class CourierSerializer(serializers.ModelSerializer):
    class Meta:
        model = Waybill
        fields = [
                 'id'        ,
                 'posting_data'   ,
                 'customer_name',
                 'phonenumber',
                 'phonenumber_tow', 
                 'country'      ,
                 "city" ,
                 "area" ,
                 "street" ,
                 "building_number" ,
                 "address_one" ,
                 "address_tow" , 
                 "delivery_fees" ,
                 "order_fees" ,
                 "grand_total" ,
                 "waybill_status" ,
                 "item"  ]


class WaybillCreatedList(viewsets.ViewSet):

    def list(self, request):
        print(request.method)
        queryset =Waybill.objects.filter(waybill_status="Created")
        serializer = CourierSerializer(queryset, many=True)
        return Response(serializer.data)
    def list_updated(self, request):
        now = datetime.datetime.now()
        filterd_time = now - datetime.timedelta(minutes=60)
        queryset =Waybill.objects.filter(updated_date__gte=filterd_time)
        serializer = CourierSerializer(queryset, many=True)
        return Response(serializer.data)
